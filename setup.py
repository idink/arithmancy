from setuptools import setup, find_packages

setup(
      name='arithmancy',
      version='0.1',
      description='a collection of statistical analysis methods',
      url='',
      author='Idin',
      author_email='d@idin.net',
      license='',
      packages=find_packages(),
      zip_safe=False
)